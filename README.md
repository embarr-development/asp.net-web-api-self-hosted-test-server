# ASP.Net Web API Self Hosted Test Server #

This is a simple class for spinning up an ASP.Net Web API self host. 
I use it regularly within my test code.

It uses the Microsoft ASP.Net Web API 2.2 OWIN Self Host library.

## Example Usage ##

	 Host.StartHost<DefaultHost>();
	 
	 Host.StopHost();
	 
## How I use it ##
I quite often use it within a SpecFlow hook like the following:


    public class WebApiHooks
    {
        [BeforeFeature("DefaultHost")]
        public static void BeforeDefaultHostFeature()
        {
            Host.StartHost<DefaultHost>(getBaseAddress: SetBaseAddress);
        }

        [AfterFeature("DefaultHost")]
        public static void AfterDefaultHostFeature()
        {
            Host.StopHost();
        }

        private static void SetBaseAddress(string baseAddress)
        {
            FeatureContextService.SaveValue("baseAddress", baseAddress);
        } 
    }
	
* The SetBaseAddress method is passed in to get the Web API url for the test feature.
* You can pass the port in, otherwise an available port it selected.
* *NOTE: you would need a [Binding] attribute above the WebApiHooks class, I had to remove it as it messes up the page formatting*

## Default Host Example ##
Default host normally looks something like:

    public class DefaultHost
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );

            appBuilder.UseWebApi(config);
        }
    }